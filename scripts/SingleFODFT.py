from aimsutils import aims_calc_params
from aimsutils.fodft.aims import FoAims
from ase import io


def prepare_aims(binary_in, specpath_in, ncpu_in, xc_in="blyp",
                 species_in="tight"):
    """
    Prepare the aims_params dict for the ASE aims calculator.
    """
    aims_params = aims_calc_params(binary_path=binary_in,
                                   species_path=specpath_in,
                                   xc=xc_in,
                                   species=species_in,
                                   mpiexe='mpirun',
                                   numcpu=ncpu_in)
    aims_params["packed_matrix_format"] = "index"
    return aims_params


if __name__ == '__main__':
    # default fodft_binary for jessie image
    aims_binary = '/data/schober/aims.fodft.x'

    # TODO CHANGE: path to species defaults
    aims_species = '/data/schober/code/fhiaims_develop/fhiaims_supporting_work/species_defaults/'

    # prepare aims parameters for the ASE calculator
    aims_params = prepare_aims(aims_binary, aims_species, ncpu_in=4)

    # read in fragment files
    f1 = io.read("frag1.in")
    f2 = io.read("frag2.in")

    # instantiate the FoAims calculator with necessary detaisl
    fo_calc = FoAims(fragment1=f1, fragment2=f2, charges=[+1, 0], fo_type='hole', **aims_params)

    # do the calculations and retrieve H_ab
    folder, hab, states = fo_calc.do_calculations()
