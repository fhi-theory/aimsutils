#!/usr/bin/env python

# Take an aims restart file (cluster or periodic) and convert it.
# Works only for gamma only periodic restarts.

from aimsutils import data
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Convert an FHIaims restart\
file from cluster to periodic or vice versa. Works only for gamma only\
calculations.")

    parser.add_argument("outputfile", help="The FHIaims output file", type=str)

    arg = parser.parse_args()

    print("Reading FHIaims restart.")
    meta = data.parse_aimsout(arg.outputfile)
    eigenvector, eigenvalue, occupation = data.read_restart(arg.outputfile)

    # invert periodicity status
    per = not meta['periodic']

    print("Writing FHIaims restart.")
    data.write_restart(meta['restartfile'],
                       per,
                       eigenvector,
                       eigenvalue,
                       occupation)
    print("Done.")
