#!/usr/bin/env python

from setuptools import find_packages
from numpy.distutils.core import setup, Extension

setup(
    name="aimsutils",
    version="0.1",
    description="Collection of Python utitlities for FHIaims",
    author="Many.",
    maintainer="Christoph Schober <christoph.schober@ch.tum.de>",
    author_email="christoph.schober@ch.tum.de",
    url="https://gitlab.lrz.de/theochem/aimsutils",
    packages=find_packages(),
    ext_modules=[Extension(name="restartutils",
                           sources=["source/restartutils.f90"])],
    requires=['ase',
              'collections',
              'copy',
              'math',
              'numpy',
              'os',
              're',
              'scipy',
              'shutil',
              'spherical_functions',
              'quaternion',
              'sys'],
    license='lgpl-3.0',
    scripts=['scripts/ConvertRestart.py',
             'scripts/RotateSingle.py',
             'scripts/RotateMany.py']

    )
