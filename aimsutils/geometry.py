import numpy as np
from collections import namedtuple, Counter
import hashlib
from scipy.stats import moment
from copy import deepcopy
from scipy.spatial.distance import cdist
import ase
from aimsutils import deprecated


dep_msg = "The geometry module with all functionality was moved to the new \
orgel_base package and might be removed from this package in the future. \
Please adjust your code and scripts accordingly."


@deprecated(dep_msg)
def build_supercell(systems):
    """
    Combine all systems into a single atoms object.

    Parameters
    ----------
    systems :  List of ``ase.atoms.Atoms``-objects
        All systems which should be combined into one cell.

    Returns
    -------
    supercell : ``ase.atoms.Atoms``-object
        Single cell with all systems.
    """
    supercell = deepcopy(systems[0])
    if len(systems) > 1 and not isinstance(systems, ase.atoms.Atoms):
        for sys in systems[1:]:
            supercell += sys

    return supercell


@deprecated(dep_msg)
def get_closest_system(systems, coordinates='COM'):
    """
    Out of a list with multiple atoms objects, return the system with its
    center-of-mass closest to the point defined by coordinates.

    Parameters
    ----------
    systems : List of ``ase.atoms.Atoms``-objects
        All systems which are part of the supercell.
    coordinates : ``np.array`` or ``[x,y,z]``
        np.array or list of xyz coordinates.
        Special: Use ``'COM'`` for the center-of-mass of the supercell.

    Returns
    -------
    closest : int
        Index of the closest molecule in the input list.
    """
    supercell = build_supercell(systems)

    if coordinates == "COM":
        coordinates = supercell.get_center_of_mass()
    elif isinstance(coordinates, str) and coordinates != "COM":
        raise ValueError("Only 'COM' is a valid option string.")

    coms = [x.get_center_of_mass() for x in systems]
    dists = [np.linalg.norm(coordinates-x) for x in coms]
    closest = np.argsort(dists)[0]

    return closest


@deprecated(dep_msg)
def get_nearest_neighbours(systems, cutoff=None, nn_scaling=1.5,
                           center="COM", sortby='COM', return_dists=False):
    """
    Get the nearest neighbours for the molecule closest to ``center`` position.

    Parameters
    ----------
    systems :
        List of systems (e.g. from :function cluster:)
    cutoff : float
        Cut-Off for nearest neighbour search. If any atom of a neighbouring
        system lies within a circle of radius ``cutoff`` around ``center`` it
        is considered a hit. If no cutoff is given, the smallest distance found
        between any two molecules is used (scaled by `scaling`)
    nn_scaling : float
        Scaling factor for `cutoff`.
    sortby : str
        Either 'COM' or 'MIN'. Changes by which distance the neighbours are
        sorted.
    center : list(x,y,z) or str "COM"
        Determines the central molecule. Can be any x,y,z coordinate,
        the algorithm will look for the next molecule to this position
        and determine the nearest neighbours. If "COM" is given, the
        center of mass for all given systems combined is used as starting
        point (i.e. center of supercell/supercluster)
    return_dists : boolean
        If True, also return distance data (COM + MIN)

    Returns
    -------
    central : ase.atoms.Atoms
        The molecule for which the nearest neighbours were calculated
    neighbours : list of ase.atoms.Atoms
        All nearest neighbours to the central molecule according
        to the parameters and sorted.
    min_dists : np.array, optional
        The minimum distances between the central and the neighbour molecules.
    com_dists : np.array, optional
        The COM distances between the central and the neighbour molecules.
    com_dists_vec : np.array, optional
        The distance vectors for the COM distances between the central and
        the neighbour molecules.

    Examples
    --------

    Get all nearest neighbours relative to the central molecule in an
    antracene supercell.
    Use defaults for cutoff (minimum distance) and nn_scaling:

    >>> fragments = cluster("ANTCEN.cif",
    ...                     multiplicator=3,
    ...                     cleanup=True)
    >>> central, n_neighbours = get_nearest_neighbours(fragments,
                                                       center="COM")

    Get the nearest neighbours relative to a specific xyz-coordinate:

    >>> central, n_neighbours = get_nearest_neighbours(
        fragments, center=[5., 3., 0.])
    """
    sys = deepcopy(systems)
    central_idx = get_closest_system(sys, center)
    central_mol = sys.pop(central_idx)

    min_dists = np.array([np.min(cdist(central_mol.get_positions(),
                                       x.get_positions())) for x in sys])

    com_vec = np.array([central_mol.get_center_of_mass() -
                        x.get_center_of_mass() for x in sys])

    com_dists = np.array([np.linalg.norm(x) for x in com_vec])

    dists = {'MIN': min_dists, 'COM': com_dists}

    if cutoff is None:
        cutoff = np.min(min_dists) * nn_scaling

    idx = (min_dists < cutoff).nonzero()[0]
    if len(idx) == 0:
        raise RuntimeError('No neighbour was found within cutoff. Please try '
                           'to specify a higher cutoff value!')

    neighbours = [sys[i] for i in idx]

    sd = np.argsort(dists[sortby][idx])
    neighbours = [neighbours[i] for i in sd]

    d_data = namedtuple('data', ['central', 'neighbours', 'min_dists',
                                 'com_dists', 'com_dists_vec'])
    data = namedtuple('data', ['central', 'neighbours'])
    if return_dists:
        return d_data(central_mol, neighbours, min_dists[idx][sd],
                      com_dists[idx][sd], com_vec[idx][sd])
    else:
        return data(central_mol, neighbours)


@deprecated(dep_msg)
def calc_moment(dimer, order=6):
    """Calculate the moment up to the nth order, using the central molecule
    as reference axis.

    This vector is representative for each unique set of dimers, ignoring
    any effects of the charge on the FO-DFT couplings.
    If this is to be included, the reference axis/center-of-mass would have
    to be choosen to be the COM of the central molecule.
    """
    com = dimer.get_center_of_mass()
    rdim = dimer.get_positions() - com
    test = np.c_[rdim, dimer.get_atomic_numbers()]

    mom_vec = []
    for mom in range(order):
        mom_vec.append(moment(test, moment=mom, axis=None))

    return np.array(mom_vec)


def recursive_nn(center, frags, nn_res):
    """recursive build-up of unique dimers in crystal,
    starting from a center and moving outwards.

    Makes use of 'global' lists defined in the main scope
    of the script.

    The basic idea is to start from a central molecule and calculate all
    nearest neighbours. This is the first set of dimers. All dimers are
    tested and if they are unique (via absolute center-of-mass), the coupling
    for each dimer is identified from the screening set (using moments for
    dimer-center-of-masses).
    Then, for each neighbour in the previous set, a new set of
    nearest neighbours is defined and the same assignment procedure is run.

    o dimers: All unique dimers found in the scan
    o all_habs: The respective couplings in the same order as 'dimers'
    o moments: All moment vectors identifying the dimers

    o schematic: Representation of sites and couplings using different
                 atom types depending on the value of the coupling.
    o coms: COMs for all unique dimers found

    Parameters
    ----------
    center: list of floats
        The point at which the algorithm starts to look for dimers. Usually
        the center-of-mass of the full crystal / supercell is the best.
    frags: list of ase.Atoms-objects
        The full list of all fragments (=molecules) in the crystal. Used to
        build the dimers and look for neighbours and unique dimers.
    nn_res: collections.namedtuple
        A namedtuple to be filled with the calculated dimers and information.
        This is necessary because the function works recursive, i.e. a local
        array would be overwritten in each call.

    Examples
    --------
    A suitable namedtuple should look like this:
    >>> nn_res = namedtuple('nn_res', 'coms, dimers, moments, hashs, edges')
    >>> res_in = nn_res([], [], [], [], [])

    coms: list of COMs (of dimers)
    dimers: list of [frag1, frag2] (frag1+frag2=dimer)
    moments: list of moments
    hashs: list of hashes (per dimer)
    edges: list of edges (via COMs)
    """
    new_cen = []
    cen, nei = get_nearest_neighbours(frags, center=center)
    for n in nei:
        dim = cen+n
        com = np.around(dim.get_center_of_mass(), 8).tolist()
        # COMs are unique per dimer, but different for different
        # positions of symetrically identical dimers.
        if com not in nn_res.coms:
            mom = calc_moment(dim)
            nn_res.coms.append(com)
            new_cen.append(n.get_center_of_mass().tolist())
            nn_res.dimers.append([cen, n])
            nn_res.moments.append(mom)
            nn_res.edges.append((cen.get_center_of_mass().tolist(),
                                 n.get_center_of_mass().tolist()))
            # create a hash for the moment vector for later processing
            # rounding is to identify dimers that only differ by small
            # (numerical) errors
            nn_res.hashs.append(hashlib.md5(
                np.around(mom, 6).tostring()).hexdigest())

    # for all new neighbours, do the same search and assignment
    for ndim in new_cen[:]:
        nn_res = recursive_nn(ndim, frags, nn_res)

    return nn_res


@deprecated(dep_msg)
def find_unique_dimers(fragments, center=None):
    """
    Find all unique dimers of neighbouring molecules that can be constructed
    for the given set of fragments.
    Uses the recursive nearest-neighbour-search function (recursive_nn) to do
    all the work.

    Parameters
    ----------
    fragments: list of ase.Atoms
        List of molecules as ase.Atoms-objects.
    center: list of floats, optional
        The point at which the algorithm starts to look for dimers. Usually
        the center-of-mass of the full crystal / supercell is the best. If no
        center is defined, the COM will be used as default.

    Returns
    -------
    all_dimers: namedtuple
        A namedtuple with all identified dimers in the set. See recursive_nn
        for details on each member variable.
    unique_dimers: list of [hash, dimer]
        All unique dimers in the all_dimers set. Returns the hash of the dimer
        and the dimer as list of [frag1, frag2]-ase.Atoms-object.

    Example
    -------
    (using custom clustering to get a list of molecules for a crystal)
    >>> frags = cluster(".../ANTCEN14.cif", return_discarded=False,
                        cleanup=True, multiplicator=3)
    >>> all, unique = find_unique_dimers(frags)
    >>> len(all.dimers)
    <<< 85
    >>> len(unique)
    <<< 5
    """
    supercell = build_supercell(fragments)
    if not center:
        center = supercell.get_center_of_mass().tolist()
    # "global" arrays being filled by the recursive functions
    # =======================================================
    # Do the recursive search, assign and save for all possible
    # dimers pairs in the new supercell
    # =========================================================
    nn_res = namedtuple('nn_res', 'coms, dimers, moments, hashs, edges')
    res_in = nn_res([], [], [], [], [])
    all_dimers = recursive_nn(center, fragments, res_in)

    # find all unique dimers using the hashs
    c = Counter(all_dimers.hashs)
    unique_dimers = []
    for h in c.iterkeys():
        for i in range(len(all_dimers.dimers)):
            if all_dimers.hashs[i] == h:
                unique_dimers.append([all_dimers.hashs[i],
                                      all_dimers.dimers[i]])
                break

    return all_dimers, unique_dimers
