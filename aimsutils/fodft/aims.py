from aimsutils.fodft import FoBase, get_habs
from ase.calculators.aims import Aims
import os
import json
import glob


class FoAims(FoBase):
    """
    Implements a single FO-DFT calculation using FHIaims and the ASE FileIO
    FHIaims calculator.
    """
    def __init__(self, fo_orbs=None, fo_range='1 1', fo_flavour=None,
                 *args, **kwargs):
        self.__doc__ += "\n" + FoBase.__init__.__doc__
        FoBase.__init__(self, *args, **kwargs)

        self.setup_folders()
        self.fo_orb = fo_orbs
        self.fo_range = fo_range
        self.results = None
        kwargs.pop('fragment1')
        kwargs.pop('fragment2')
        kwargs.pop('charges')
        kwargs.pop('fo_type')

        # Change CK to determine sign of WF
        if fo_orbs!=None:
            if len(fo_orbs) > 1:
                kwargs['output cube eigenstate'] = fo_orbs.split()[0]

        self.f1.set_calculator(Aims(
            charge=self.f1_params['charge'],
            default_initial_moment=self.f1_params['initial_moment'],
            fo_dft='fragment',
            **kwargs))

        # Change CK to determine sign of WF
        if fo_orbs!=None:
            if len(fo_orbs) > 1:
                kwargs.pop('output cube eigenstate')
                kwargs['output cube eigenstate'] = fo_orbs.split()[1]

        self.f2.set_calculator(Aims(
            charge=self.f2_params['charge'],
            default_initial_moment=self.f2_params['initial_moment'],
            fo_dft='fragment',
            **kwargs))

        # Change CK to determine sign of WF
        if fo_orbs!=None:
            if len(fo_orbs) >1:
                kwargs.pop('output cube eigenstate')
                kwargs['fo_verbosity'] = '2'

        self.di.set_calculator(Aims(
            charge=self.di_params['charge'],
            default_initial_moment=self.di_params['initial_moment'],
            fo_dft='final',
            fo_flavour=fo_flavour,
            fo_folders=' '.join(self.folders[0:2]),
            **kwargs))

        if self.fo_orb is None:
            self.determine_fo_orbitals()
        else:
            self.fo_orb = self.fo_orb + ' ' + self.fo_range

        print(self.fo_orb)

        self.di.calc.set(fo_orbitals=self.fo_orb + ' ' + str(self.fo_type))

    def determine_fo_orbitals(self):
        """Use HOMO/LUMO and type of FO to determine the orbitals for which
        the coupling is calculated."""
        if self.fo_type == "hole":
            if self.f1_params['charge'] > 0:
                self.fo_orb = "{f1} {f2} {r}".format(f1=self.f1_params['lumo'],
                                                     f2=self.f2_params['homo'],
                                                     r=self.fo_range)
            if self.f2_params['charge'] > 0:
                self.fo_orb = "{f1} {f2} {r}".format(f1=self.f1_params['homo'],
                                                     f2=self.f2_params['lumo'],
                                                     r=self.fo_range)

            if self.f1_params['charge'] == 0 and self.f2_params['charge'] == 0:
                self.fo_orb = "{f1} {f2} {r}".format(f1=self.f1_params['homo'],
                                                     f2=self.f2_params['homo'],
                                                     r=self.fo_range)

        elif self.fo_type == "elec":
            if self.f1_params['charge'] < 0:
                self.fo_orb = "{f1} {f2} {r}".format(f1=self.f1_params['homo'],
                                                     f2=self.f2_params['lumo'],
                                                     r=self.fo_range)
            if self.f2_params['charge'] < 0:
                self.fo_orb = "{f1} {f2} {r}".format(f1=self.f1_params['lumo'],
                                                     f2=self.f2_params['homo'],
                                                     r=self.fo_range)
            if self.f1_params['charge'] == 0 and self.f2_params['charge'] == 0:
                self.fo_orb = "{f1} {f2} {r}".format(f1=self.f1_params['lumo'],
                                                     f2=self.f2_params['lumo'],
                                                     r=self.fo_range)

    def cleanup_files(self):
        """Delete all restart files after FODFT run is done."""
        files = glob.glob("f_*/restart.frag")
        files.extend(glob.glob("d_*/restart.combined"))
        files.extend(glob.glob("d_*/fo_ks_eigenvector"))

        for file in files:
            try:
                os.remove(file)
            except OSError:
                pass

    def do_calculations(self):
        """Do all calculations using the ASE aims calculator."""
        basecwd = os.getcwd()
        if not self.f1_exists:
            try:
                os.chdir(self.folders[0])
                self.f1.get_potential_energy()
            finally:
                os.chdir(basecwd)
        if not self.f2_exists:
            try:
                os.chdir(self.folders[1])
                self.f2.get_potential_energy()
            finally:
                os.chdir(basecwd)
        if not self.di_exists:
            try:
                os.chdir(self.folders[2])
                self.di.get_potential_energy()
            finally:
                os.chdir(basecwd)

        habs, states = get_habs(
            os.path.join(self.folders[2], "aims.out"))

        # ident = self.f1_unique + '_' + self.f2_unique
        ident = self.di_unique
        try:
            with open("fodft_results.json", "r") as f:
                results = json.load(f)
                results.append([ident, habs, states])
        except IOError:
            results = [[ident, habs, states]]
        finally:
            with open("fodft_results.json", "w") as f:
                json.dump(results, f)
            self.results = results

        return ident, habs, states

    def setup_folders(self, truncation=6):
        """Setup folder names for FO-DFT. Uses molecule hashes."""
        self.folders = ['f_{}'.format(self.f1_unique[:truncation]),
                        'f_{}'.format(self.f2_unique[:truncation]),
                        'd_{}'.format(self.di_unique[:truncation])]
        try:
            if not self.f1_exists:
                os.mkdir(self.folders[0])
            if not self.f2_exists:
                os.mkdir(self.folders[1])
            if not self.di_exists:
                os.mkdir(self.folders[2])
        except IOError:
            print('ATTENTION: It seems a hash collision occured. Please try to\
increate the truncation value in the setup_folders routine and see if this\
helps.')
            raise
