"""
The fodft module provides functionality to easily calculate coupling constants
with minimal human interaction.

It shall calculate all couplings for a set of molecules.
"""
import os
import numpy as np
from copy import deepcopy
from aimsutils import find_aimsout
import ase
import hashlib
import json
import base64
import re


def get_molecule_hash(system, charge=None):
    """
    Calculate an unique MD5 hash for a molecule moved to origin by COM.
    If a charge is given, the charge will be included in the hash to distinguish
    between same molecules but different charges.
    """
    work = deepcopy(system)
    com = work.get_center_of_mass()
    work.translate(com*-1)
    pos = work.get_positions()
    atoms = work.get_atomic_numbers()
    # round to eliminate very small displacements in structure
    pos = np.around(pos, 6)

    # set all 0/-0 to explicit unsigned 0
    pos[pos == 0] = 0
    work = np.c_[pos, atoms]

    if charge is not None:
        work = np.vstack([work, 4*[charge]])

    hashed = hashlib.md5(work).digest()
    hashed = base64.urlsafe_b64encode(hashed).decode("utf-8")
    
    return hashed


class FoBase(object):
    """
    The base class to implement a single FO-DFT calculation of any kind.
    """
    def __init__(self, fragment1, fragment2, charges, fo_type,
                 initial_moments=None, frontier_orbitals=None,
                 *args, **kwargs):
        """
        Parameters
        ----------
        fragment1: file or ase.atoms.Atoms
            The first fragment.
        fragment2: file or ase.atoms.Atoms
            The second fragment.
        charges: list
            The charges for each fragment, e.g. [1, 0] or [-1, 0] etc.
            Optional: 3rd int with charge for dimer
            (used for H(2n-1)@DA schemes)
        fo_type: str
            Whether it is a hole or electron FO-DFT (determines
            which frontier orbitals to be used)

        Returns
        -------

        """
        if not self._is_atoms_obj(fragment1):
            fragment1 = ase.io.read(fragment1)
        if not self._is_atoms_obj(fragment2):
            fragment2 = ase.io.read(fragment2)

        # Toggles for existing calculations (book-keeping)
        self.di_exists = False
        self.f1_exists = False
        self.f2_exists = False

        self.identifier = ['f1', 'f2', 'di']
        self.folders = []

        self.f1 = fragment1
        self.f2 = fragment2
        self.di = fragment1 + fragment2

        self.fo_type = fo_type

        self.f1_params = {}
        self.f2_params = {}
        self.di_params = {}

        self.setup_charges(charges)
        if not initial_moments:
            self.setup_initial_moments()

        if not frontier_orbitals:
            self.setup_frontier_orbitals()

        self.read_hashs()
        self.write_hashs()
        self.check_calc_status()

    def check_calc_status(self):
        """Check if calculations of this dimer, fragment1 or fragment2
        already exist.
        If fragment exists, re-use density.
        If dimer exists, re-use coupling.
        """
        if self.di_unique in self.fodata['di'][:-1]:
            self.di_exists = True
        if self.f1_unique in self.fodata['f1'][:-1] + self.fodata['f2'][:-1]:
            self.f1_exists = True
        if self.f2_unique in self.fodata['f1'][:-1] + self.fodata['f2'][:-1]:
            self.f2_exists = True

        # special case if fragments in this calc are identical
        if self.f1_unique == self.f2_unique:
            self.f2_exists = True

    def setup_folders():
        """Setup folder names based on fragment number."""
        raise NotImplementedError

    def read_hashs(self):
        """Read hash file, if exists, to determine existing calculations."""
        try:
            with open('fodft_data.json', 'r') as f:
                fodata = json.load(f)
        except IOError:
            fodata = {'f1': [],
                      'f2': [],
                      'di': []}
        self.fodata = fodata

    def write_hashs(self):
        """Write the molecule hash to a file."""
        for obj in self.identifier:
            system = getattr(self, obj)
            params = getattr(self, '{}_params'.format(obj))
            setattr(self, '{}_unique'.format(obj),
                    '{}'.format(get_molecule_hash(system, params['charge'])))

            self.fodata[obj].append(get_molecule_hash(system, params['charge']))
            with open('fodft_data.json', 'w') as f:
                json.dump(self.fodata, f)

    def setup_charges(self, charges):
        """Determine charges for all fragments and dimer."""
        self.f1_params['charge'] = charges[0]
        self.f2_params['charge'] = charges[1]
        try:
            self.di_params['charge'] = charges[2]
        except IndexError:
            self.di_params['charge'] = sum(charges)

    def setup_initial_moments(self):
        """Determine initial moments based on charges for low spin systems!"""
        self.f1_params['initial_moment'] = self.get_initial_moment(
            self.f1,
            self.f1_params['charge'])
        self.f2_params['initial_moment'] = self.get_initial_moment(
            self.f2,
            self.f2_params['charge'])
        self.di_params['initial_moment'] = self.get_initial_moment(
            self.di,
            self.di_params['charge'])

    def return_parameter(self, param):
        """Returns list with parameter for f1, f2, di."""
        par_list = []
        par_list.append(self.f1_params[param])
        par_list.append(self.f2_params[param])
        par_list.append(self.di_params[param])
        return par_list

    def setup_frontier_orbitals(self):
        """
        Extrapolate the frontier orbitals (HOMO+LUMO) based on the number of
        electrons and integer occupation numbers.
        Only for charges of +- 1.

        Sign of orbital determines spin channel (+ = up, - = down)
        """
        for obj in self.identifier:
            system = getattr(self, obj)
            params = getattr(self, '{}_params'.format(obj))
            charge = params['charge']

            n_elec = system.get_atomic_numbers().sum()

            if charge > 0:
                params['homo'] = n_elec / 2  # spin up
                params['lumo'] = n_elec / 2  # spin down
            elif charge < 0:
                params['homo'] = n_elec / 2 + 1  # spin up
                params['lumo'] = n_elec / 2 + 1  # spin down
            elif charge == 0:
                params['homo'] = n_elec / 2  # spin up
                params['lumo'] = n_elec / 2 + 1  # spin down or spin up

    @staticmethod
    def get_initial_moment(system, charge):
        """
        Calculate the initial moment for a given system. At the moment,
        only implemented for low spin systems.

        Parameters
        ----------
        system : ase.Atoms-object
            The system for which the initial moment will be calculated
        charge : int
            The charge of the system

        Returns
        -------
        initial_moment : int
            The initial moment.
        """
        e_number = system.get_atomic_numbers().sum() - charge

        return e_number % 2

    @staticmethod
    def _is_atoms_obj(item):
        """
        Returns True if the given item is no list, but an atoms object.
        """
        # check for ase.Atoms-object is better than not_list here.
        if isinstance(item, ase.Atoms):
            return True
        elif not os.path.isfile(item):
            raise IOError("'{}' is not a valid file!".format(item))


def get_habs(outfile):
    """
    Return coupling values from FHI-aims output

    Parameters
    ----------
    outfile : string
        Path to the output file or the calculation folder.

    Returns
    -------
    habs : list
        List of all coupling values
    states : list
        List of all states (frag1 -> frag2) for the couplings
    """

    outfile = find_aimsout(outfile)

    with open(outfile, "r") as f:
        out = f.readlines()

    p = re.compile("\d{3} -> \d{3}:\s+ [-\d.\d]*")
    hitlist = re.findall(p, str(out))
    states = list()
    habs = list()

    for hit in hitlist:
        t_states = hit.split(":")[0].strip()
        states.append([int(x.strip()) for x in t_states.split("->")])
        habs.append(float(hit.split(":")[1].strip()))

    return habs, states
