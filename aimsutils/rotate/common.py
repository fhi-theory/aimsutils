import numpy as np
from aimsutils.rotate.wigner_fast import construct_np_C

# tabulated arrays for aims YLM rotation.

# All values can be re-calculated using the indicated functions before the
# array.

# see aimsutils.ylms
T = np.array([[1],
             [1, 1, -1],
             [1, 1, 1, -1, 1],
             [1, 1, 1, 1, -1, 1, -1],
             [1, 1, 1, 1, 1, -1, 1, -1, 1],
             [1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1],
             [1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1],
             [1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, -1],
             [1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, -1, 1],
             [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1],
             [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1,
              1],
             [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, -1, 1,
              -1, 1, -1],
             [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, -1, 1,
              -1, 1, -1, 1]])

# array to split the continuous WignerD matrix up to l = 9
lsplit = [1, 10, 35, 84, 165, 286, 455, 680, 969, 1330]


C = [np.nan for i in range(len(T))]
C_a = [np.nan for i in range(len(T))]
for i in range(len(T)):
    Ce = construct_np_C(i)
    C_a[i] = np.diag(T[i]).dot(Ce)
    C[i] = Ce
