# This file is part of aimsutils
# (C) 2015 Christoph Schober

import sys
from collections import OrderedDict

import numpy as np
import sympy
from numpy import sqrt as sqrt2
from sympy import Matrix, I
from sympy.functions import sqrt
from sympy.functions.special.spherical_harmonics import Ynm
from sympy.physics.quantum.spin import Rotation


def construct_full_C(lmax):
    """
    Wrapper for construct_C to get the C matrix for all l.

    Parameters
    ----------
    l : int
        Angular momentum l.

    Returns
    -------
    C_all : OrderedDict
        OrderedDict with all C matrices to transform complex to real YLMs.
    """
    C_all = OrderedDict()
    for l in np.arange(0, lmax+1):
        C_all[l] = construct_C(l)
    return C_all


def construct_C(l):
    """
    Construct the C matrix according to Blanco et al (eq. 19).

    Parameters
    ----------
    l : int
        Angular momentum l.

    Returns
    -------
    C : sympy.Matrix
        The C matrix to transform complex to real YLMs.
    """
    ms = np.arange(-l, l+1, 1)

    C = Matrix.zeros(len(ms))

    for m in ms:
        for m2 in ms:
            if abs(m) != abs(m2):
                element = 0
            elif m == 0 and m2 == 0:
                element = sqrt(2)
            elif abs(m) > 0:
                if np.sign(m) == np.sign(m2) == -1:
                    # upper left, m<0 + m2<0
                    element = I
                elif np.sign(m) == 1 and np.sign(m2) == -1:
                    # lower left, m>0 + m2<0
                    element = 1
                elif np.sign(m) == 1 == np.sign(m2) == 1:
                    # lower right, m>0 + m2>0
                    element = (-1)**(l-(l-m))
                elif np.sign(m) == -1 and np.sign(m2) == 1:
                    # upper right, m<0 + m2 > 0
                    element = -I*(-1)**(l-(l-m))

            C[m+l, m2+l] = element
    C = (1/sqrt(2))*C

    return C


def construct_Delta(alpha, beta, gamma, lmax, C):
    """
    Construct rotation matrix Delta.

    \Delta = C_a^*wigner_D*C^t

    Parameters
    ----------
    alpha, beta, gamma : float or sympy.Symbol
        Euler angles in radians (or sympy.Symbol)
    lmax : int
        Maximal angular momentum.
    C : sympy.Matrix
        The C transformation matrix (see construct_C)
    """
    Deltas = OrderedDict()
    for l in range(lmax+1):
        D = wigner_D_mat(l, alpha, beta, gamma)
        Delta = wigner_Delta(C[l], D)
        Deltas[l] = Delta
    return Deltas


def znm(theta, phi, lmax):
    """
    Construct real YLMs from complex YLMs and according to C matrix.

    Parameters
    ----------
    theta : float or sympy.Symbol
        The polar angle in radians or sympy.Symbol
    phi : float or sympy.Symbol
        The azimuthal angle in radians or sympy.Symbol
    lmax : int
        Maximal angular momentum

    Returns
    -------
    all_S : OrderedDict
        Dictionary with real YLMs for all requested l.
    """
    all_S = OrderedDict()

    for l in range(lmax+1):
        C = construct_C(l)
        ms = np.arange(-l, l+1, 1)
        Y = list()
        for m in ms:
            Y.append(Ynm(l, m, theta, phi).expand(func=True))

        if l > 0:
            all_S[l] = sympy.simplify(C.dot(Y))
        else:
            all_S[l] = [sympy.simplify(C.dot(Y))]

    return all_S


def wigner_d_mat(l, beta):
    """
    Calculate d matrix.

    Parameters
    ----------
    l : int
        Angular momentum.
    beta : float or sympy.Symbol
        The Euler angle beta in radians or sympy.Symbol

    Returns
    -------
    d_mat : sympy.Matrix
        The Wigner small d matrix.
    """
    size = l*2+1
    d_mat = Matrix.zeros(size)

    # iterate over matrix
    for row in range(size):
        for col in range(size):
            d_mat[row, col] = Rotation.d(l,
                                         row-l,
                                         col-l,
                                         beta).doit().simplify()

    return d_mat


def wigner_D_mat(l, alpha, beta, gamma):
    """
    Calculate D matrix for given angular momentum l and Euler angles
    alpha, beta and gamma.

    Parameters
    ----------
    l : int
        Angular momentum.
    alpha, beta, gamma : float
        Euler angles for Wigner D matrix in radians or symbolic (sympy.Symbol).

    Returns
    -------
    D_mat : sympy.Matrix
        The Wigner-D matrix for given l.
    """
    size = l*2+1
    D_mat = Matrix.zeros(size)

    # iterate over matrix
    for row in range(size):
        for col in range(size):
            D_mat[row, col] = Rotation.D(l, row-l, col-l,
                                         alpha, beta, gamma).doit().simplify()

    return D_mat


def wigner_Delta(C, D):
    """
    Construct the real rotation matrix for a given angular momentum.
    The l value is implicitly given via the dimensions of the matrices.

    Parameters
    ----------
    C : sympy.Matrix
        The transformation matrix between complex and real YLMs.
    D : sympy.Matrix
        The Wigner-D matrix for one angular momentum (m x m).

    Returns
    -------
    D2_mat : sympy.Matrix
        The real rotation matrix for rotations of YLM coefficients for the
        angular momentum l.
    """
    D2_mat = C.conjugate()*D*C.T

    # evalf() is ok for symbolic equations as well, the danger is to loose
    # accuracy due to standard floating point issues. Might add a check here.

    if not D2_mat.is_symbolic():
        cmplx_residue = 0
        ms = range(D2_mat.shape[0])
        for m in ms:
            for m2 in ms:
                value = D2_mat[m, m2].evalf().as_real_imag()
                D2_mat[m, m2] = value[0]
                cmplx_residue += abs(value[1])
        if cmplx_residue > 1E-5:
            sys.exit("Numeric residue of complex values > 1E-5. This means\
                     the rotation matrix is in part imaginary. Please check\
                     inputs carefully!")

    return D2_mat

