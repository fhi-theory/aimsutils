#!/usr/bin/env python
import unittest
# from aimsrotate.aims import data, utils
# from aimsrotate import wigner, rotate
import os
import tempfile
import shutil

from aimsutils import restarts


class TestRestarts(unittest.TestCase):

    def setUp(self):
        self.cwd = os.getcwd()
        self.tmpdir = tempfile.mkdtemp()
        os.chdir(self.tmpdir)
        self.files = {}
        self.outfiles = {}
        self.files['cluster_1'] = os.path.join(self.cwd, 'tests', 'data',
                                               'h2o.cluster.1',
                                               'restart.f')
        self.files['cluster_2'] = os.path.join(self.cwd, 'tests', 'data',
                                               'h2o.cluster.2',
                                               'restart.f')
        self.files['periodic_1'] = os.path.join(self.cwd, 'tests', 'data',
                                                'h2o.periodic.1',
                                                'restart.f')
        self.files['periodic_2'] = os.path.join(self.cwd, 'tests', 'data',
                                                'h2o.periodic.2',
                                                'restart.f')

        self.outfiles['cluster_1'] = os.path.join(self.cwd, 'tests', 'data',
                                                  'h2o.cluster.1',
                                                  'aims.out')
        self.outfiles['cluster_2'] = os.path.join(self.cwd, 'tests', 'data',
                                                  'h2o.cluster.2',
                                                  'aims.out')
        self.outfiles['periodic_1'] = os.path.join(self.cwd, 'tests', 'data',
                                                   'h2o.periodic.1',
                                                   'aims.out')
        self.outfiles['periodic_2'] = os.path.join(self.cwd, 'tests', 'data',
                                                   'h2o.periodic.2',
                                                   'aims.out')

    def test_spin_none_cluster_restart(self):
        ks_ev, ks_e, occ = restarts.read_restart(self.outfiles['cluster_1'])

    def test_spin_none_periodic_restart(self):
        ks_ev, ks_e, occ = restarts.read_restart(self.outfiles['periodic_1'])

    def test_spin_collinear_cluster_restart(self):
        ks_ev, ks_e, occ = restarts.read_restart(self.outfiles['cluster_2'])

    def test_spin_collinear_periodic_restart(self):
        ks_ev, ks_e, occ = restarts.read_restart(self.outfiles['periodic_2'])

    def tearDown(self):
        os.chdir(self.cwd)
        shutil.rmtree(self.tmpdir)
