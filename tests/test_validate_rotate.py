#!/usr/bin/env python
import os
import unittest
import numpy as np
import shutil
import tempfile
os.environ["NUMBA_DISABLE_JIT"] = "1"
from aimsutils.rotate.rotate2 import Rotations
import restartutils


class RotationBasic(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cwd = os.getcwd()
        cls.c1 = os.path.join(cls.cwd, 'tests', 'data',
                              'h2o.cluster.1')
        cls.p1 = os.path.join(cls.cwd, 'tests', 'data',
                              'h2o.periodic.1')
        cls.c2 = os.path.join(cls.cwd, 'tests', 'data',
                              'h2o.cluster.2')
        cls.p2 = os.path.join(cls.cwd, 'tests', 'data',
                              'h2o.periodic.2')
        cls.ex1 = os.path.join(cls.cwd, 'tests', 'data',
                               'h2o_c1')
        cls.ex2 = os.path.join(cls.cwd, 'tests', 'data',
                               'h2o_c2')
        cls.ex3 = os.path.join(cls.cwd, 'tests', 'data',
                               'h2o_c1_c1minimal')


class TestRotationClusterSpinNone(RotationBasic):
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        os.chdir(self.tmpdir)

        self.r = Rotations()
        self.r.add_rotation(self.c1, [0, 0, 0])
        self.r.add_rotation(self.c1, [90, 113, 85], [0, 4.3, 0])
        self.r.add_rotation(self.c1, [215, 22, 0], [0, 0, 4])
        self.r.title = 'test_ex1'
        self.r.write_restartfile()
        self.ev, self.e, self.occ = restartutils.read_restart_cluster(
            24, 129, 1,
            os.path.join("test_ex1", "restart.combined"))

        self.ref_ev, self.ref_e, self.ref_occ = restartutils.read_restart_cluster(
            24, 129, 1,
            os.path.join(self.ex1, "restart.combined"))

    def tearDown(self):
        os.chdir(self.cwd)
        shutil.rmtree(self.tmpdir)

    def test_validate_eigenvector(self):
        self.assertTrue(np.allclose(self.ref_ev, self.ev))

    def test_validate_eigenvalues(self):
        self.assertTrue(np.allclose(self.ref_e, self.e))

    def test_validate_occupations(self):
        self.assertTrue(np.allclose(self.ref_occ, self.occ))


class TestRotationClusterSpinCollinear(RotationBasic):
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        os.chdir(self.tmpdir)

        self.r = Rotations()
        self.r.add_rotation(self.c2, [0, 0, 0])
        self.r.add_rotation(self.c2, [90, 113, 85], [0, 4.3, 0])
        self.r.add_rotation(self.c2, [215, 22, 0], [0, 0, 4])
        self.r.title = 'test_ex2'
        self.r.write_restartfile()
        self.ev, self.e, self.occ = restartutils.read_restart_cluster(
            24, 129, 2,
            os.path.join("test_ex2", "restart.combined"))

        self.ref_ev, self.ref_e, self.ref_occ = restartutils.read_restart_cluster(
            24, 129, 2,
            os.path.join(self.ex2, "restart.combined"))

    def tearDown(self):
        os.chdir(self.cwd)
        shutil.rmtree(self.tmpdir)

    def test_validate_eigenvector(self):
        self.assertTrue(np.allclose(self.ref_ev, self.ev))

    def test_validate_eigenvalues(self):
        self.assertTrue(np.allclose(self.ref_e, self.e))

    def test_validate_occupations(self):
        self.assertTrue(np.allclose(self.ref_occ, self.occ))
