import unittest

from ase import Atoms
from aimsutils.geometry import get_closest_system, build_supercell
from aimsutils.geometry import get_nearest_neighbours


class TestVarious(unittest.TestCase):
    """
    Test various functions from ase.utils.
    """
    def setUp(self):
        self.sys = Atoms('H12', [(-9.92572, 2.02070, -0.18348),
                                 (-9.89061, 1.31393, -0.16049),
                                 (-7.82599, 4.43760,  0.03185),
                                 (-7.38718, 4.99112, -0.01736),
                                 (-5.99431, 2.96173, -1.56864),
                                 (-5.50510, 2.90516, -2.07705),
                                 (-8.05968, 1.29395, -2.16975),
                                 (-8.18470, 0.91930, -2.75712),
                                 (-8.36167, 3.91920, -2.58953),
                                 (-8.21908, 4.58480, -2.78366),
                                 (-7.22447, 1.83546, 0.50914),
                                 (-7.48921, 1.88220, 1.16407)])

        self.mols = [Atoms('H2', [(-9.92572, 2.02070, -0.18348),
                                  (-9.89061, 1.31393, -0.16049)]),
                     Atoms('H2', [(-7.82599, 4.43760,  0.03185),
                                  (-7.38718, 4.99112, -0.01736)]),
                     Atoms('H2', [(-5.99431, 2.96173, -1.56864),
                                  (-5.50510, 2.90516, -2.07705)]),
                     Atoms('H2', [(-8.05968, 1.29395, -2.16975),
                                  (-8.18470, 0.91930, -2.75712)]),
                     Atoms('H2', [(-8.36167, 3.91920, -2.58953),
                                  (-8.21908, 4.58480, -2.78366)]),
                     Atoms('H2', [(-7.22447, 1.83546, 0.50914),
                                  (-7.48921, 1.88220, 1.16407)])]

    def tearDown(self):
        pass

    def test_get_closest_system_to_COM(self):
        idx = get_closest_system(self.mols, coordinates='COM')
        self.assertEqual(idx, 5)

    def test_get_closest_system_to_coord(self):
        idx = get_closest_system(self.mols,
                                 coordinates=[-8.0, 1.0, -2.5])
        self.assertEqual(idx, 3)

    def test_nonsense_str_for_coordinates(self):
        with self.assertRaises(ValueError):
            get_closest_system(self.sys, coordinates="COP")

    def test_build_supercell(self):
        sc = build_supercell(self.mols)
        self.assertEqual(sc, self.sys)

    def test_basic_neighbour_search(self):
        c, nn, min_d, com_d, com_d_v = get_nearest_neighbours(
            self.mols, cutoff=3, return_dists=True)
        self.assertEqual(len(nn), 4)
        self.assertListEqual(c.get_chemical_symbols(), ['H', 'H'])
        self.assertAlmostEqual(sum(min_d), 11.023068527166346,
                               places=10)

    def test_empty_neighbour_list(self):
        with self.assertRaises(RuntimeError):
            central, nn_list, m, c, cv = get_nearest_neighbours(
                self.mols, cutoff=2, return_dists=True)
