#!/usr/bin/env python
import unittest
# from aimsrotate.aims import data, utils
# from aimsrotate import wigner, rotate
import os
import tempfile
import shutil

from aimsutils import ylms, parser


class TestGeneralRotationComponents(unittest.TestCase):
    def setUp(self):
        self.cwd = os.getcwd()
        self.tmpdir = tempfile.mkdtemp()
        os.chdir(self.tmpdir)
        self.outfile = os.path.join(self.cwd, 'tests', 'data',
                                    'h2o.periodic.1',
                                    'aims.out')
        self.main_folder = os.path.join(self.cwd, 'tests', 'data',
                                        'h2o.periodic.1')
        self.basis = parser.parse_basis(os.path.join(self.main_folder,
                                                     "basis-indices.out"))
        self.lvec = ylms.get_l_vector(self.basis)

    def test_aims_ylm_transformation_matrix(self):
        T2_ref = [[1], [1, 1, -1], [1, 1, 1, -1, 1]]
        T3_ref = [[1], [1, 1, -1], [1, 1, 1, -1, 1], [1, 1, 1, 1, -1, 1, -1]]
        T5_ref = [[1],
                  [1, 1, -1],
                  [1, 1, 1, -1, 1],
                  [1, 1, 1, 1, -1, 1, -1],
                  [1, 1, 1, 1, 1, -1, 1, -1, 1],
                  [1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1]]

        T2 = ylms.get_num_T(2).values()
        T3 = ylms.get_num_T(3).values()
        T5 = ylms.get_num_T(5).values()

        self.assertEqual(T2, T2_ref)
        self.assertEqual(T3, T3_ref)
        self.assertEqual(T5, T5_ref)

    def tearDown(self):
        os.chdir(self.cwd)
        shutil.rmtree(self.tmpdir)
