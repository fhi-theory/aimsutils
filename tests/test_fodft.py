import os
import unittest
from ase.atoms import Atoms
from aimsutils.fodft import FoBase
from aimsutils.fodft.aims import FoAims
import tempfile
import shutil
import glob


class TestGeneralFO(unittest.TestCase):
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.cwd = os.getcwd()
        os.chdir(self.tmpdir)

        self.f1 = Atoms('He', positions=[(0, 0, 0)])
        self.f2 = Atoms('Ne', positions=[(4., 0, 0)])

        self.fo_base = FoBase(self.f1, self.f2, [1, 0], 'hole',
                              initial_moments=None)

    def test_manual_charges(self):
        reference_charges = [1, 1, 1]
        fo = FoBase(self.f1, self.f2, [1, 1, 1], 'hole',
                    initial_moments=None)
        charges = fo.return_parameter('charge')
        self.assertEqual(charges, reference_charges)

    def test_automatic_charges(self):
        reference_charges = [1, 0, 1]
        charges = self.fo_base.return_parameter('charge')
        self.assertEqual(charges, reference_charges)

    def test_initial_moments(self):
        reference_initial_moments = [1, 0, 1]
        initial_moments = self.fo_base.return_parameter('charge')
        self.assertEqual(initial_moments, reference_initial_moments)

    def test_homo(self):
        reference_homo = [1, 5, 6]
        homo = self.fo_base.return_parameter('homo')
        self.assertEqual(homo, reference_homo)

    def test_lumo(self):
        reference_lumo = [1, 6, 6]
        lumo = self.fo_base.return_parameter('lumo')
        self.assertEqual(lumo, reference_lumo)

    def tearDown(self):
        os.chdir(self.cwd)
        shutil.rmtree(self.tmpdir)

class TestBookKeeping(unittest.TestCase):
    def setUp(self):
        self.f1 = Atoms('CO', positions=[(0, 0, 0), (3, 0, 0)])
        self.f2 = Atoms('CO', positions=[(0, 4.234123, 0), (3, 4.234123, 0)])
        self.f3 = Atoms('CO', positions=[(0, 0, 0), (2.321, 0, 0)])

        self.tmpdir = tempfile.mkdtemp()
        self.cwd = os.getcwd()
        os.chdir(self.tmpdir)

    def test_folders_created(self):
        test1 = FoAims(fragment1=self.f1, fragment2=self.f2, charges=[1, 0],
                       fo_type='hole')
        frags = glob.glob('f_*')
        dimers = glob.glob('d_*')

        self.assertEqual(len(frags), 2)
        self.assertEqual(len(dimers), 1)

    def test_duplicate_is_found(self):
        test1 = FoAims(fragment1=self.f1, fragment2=self.f2, charges=[1, 0],
                       fo_type='hole')
        test2 = FoAims(fragment1=self.f1, fragment2=self.f3, charges=[1, 0],
                       fo_type='hole')

        frags = glob.glob('f_*')
        dimers = glob.glob('d_*')

        self.assertEqual(len(frags), 3)
        self.assertEqual(len(dimers), 2)

    def tearDown(self):
        os.chdir(self.cwd)
        shutil.rmtree(self.tmpdir)
