#!/usr/bin/env python
import os
import unittest
# from aimsrotate.aims import data, utils
# from aimsrotate import wigner, rotate
import numpy as np
os.environ["NUMBA_DISABLE_JIT"] = "1"
from aimsutils.rotate.rotate2 import Rotator
from numpy.random import RandomState


class TestRotator(unittest.TestCase):
    def setUp(self):
        self.in_euler = [140, 43.2, 90]
        self.in_quaternion = np.quaternion(-0.392940522273469,
                                           0.155576158559671,
                                           0.333634148697507,
                                           -0.842663669364094)
        self.prng = RandomState(12345)

        self.ks = self.prng.rand(8,4,1)
        self.lvec = [0, 1, 1, 1, 0, 1, 1, 1]

    def tearDown(self):
        pass

    def test_init_using_euler(self):
        rot = Rotator(3, *self.in_euler)
        self.assertAlmostEqual(rot.R, self.in_quaternion)

    def test_init_using_quaternion(self):
        rot = Rotator(3, self.in_quaternion)
        self.assertAlmostEqual(rot.R, self.in_quaternion)

    def test_equivalence_between_euler_and_quaternion(self):
        rotE = Rotator(3, *self.in_euler)
        rotQ = Rotator(3, self.in_quaternion)
        self.assertAlmostEqual(rotE.R, rotQ.R)

    def test_Delta_matrices(self):
        rot = Rotator(3, 10, 20, 30)

        delta3 = [np.array([[ 1.]]), np.array([[ 0.77128058,  0.05939117,
        -0.63371836], [ 0.17101007,  0.93969262,  0.29619813], [ 0.61309202,
        -0.33682409,  0.71461018]]), np.array([[ 0.16263728,  0.22337362,
        0.0346486 , -0.25589315,  0.92572756], [-0.12007964,  0.73492316,
        0.0966648 , -0.57790891, -0.31960294], [-0.08773333,  0.2783352 ,
        0.82453333,  0.48209071,  0.05065286], [-0.30380225,  0.51851774,
        -0.54821333,  0.57174724,  0.10682129], [-0.92689646, -0.25231142,
        0.09519624, -0.20306064,  0.16403032]]), np.array([[ -4.56105916e-01,
        -3.29844211e-04,   5.83392759e-02, 1.58148496e-02,  -9.51311085e-02,
        3.93815850e-01, -7.90046108e-01], [  1.34888352e-01,   1.30426506e-01,
        3.37294060e-01, 7.28042118e-02,  -3.70833412e-01,   7.55905144e-01,
        3.69886897e-01], [ -1.84818335e-02,  -1.83169065e-01,   6.82362160e-01,
        1.24205946e-01,  -4.99728017e-01,  -4.72114874e-01, -1.11542524e-01], [
        3.16296993e-02,  -1.84346735e-01,   3.57636767e-01, 6.64884733e-01,
        6.19445051e-01,   1.06432637e-01, 1.84090138e-17], [  1.04815686e-01,
        -4.24408696e-01,   3.88127875e-01, -7.04406922e-01,   3.75743288e-01,
        1.43138829e-01, -1.96679564e-02], [  3.70602701e-01,  -7.61538178e-01,
        -3.60019299e-01, 2.00027928e-01,  -2.75964180e-01,   1.37139694e-01,
        -1.34627821e-01], [  7.89998620e-01,   3.94196721e-01,   9.51311085e-02,
        -2.73921231e-02,   5.15086663e-02,  -3.29844211e-04, -4.56133333e-01]])]

        self.assertAlmostEqual(rot.Deltas[0], delta3[0])

    def test_rotated_eigenvector_using_zeros(self):
        rot = Rotator(3, 0, 0, 0)
        ks_new = rot.rotate_eigenvector(self.ks, self.lvec)
        self.assertTrue(np.allclose(self.ks, ks_new))

    def test_rotated_eigenvector_using_real_angles(self):
        ks_rot = np.array([[[ 0.92961609], [ 0.31637555], [ 0.18391881],
                            [0.20456028]],
                           [[ 0.47703791], [ 0.43069259], [ 0.59902495],
                            [0.14489612]],
                           [[ 0.80331334], [ 0.74752755], [ 0.95603927],
                            [ 1.20946048]],
                           [[ 0.10181225], [ 0.22105187], [ 0.55294451],
                            [ 0.54574451]],
                           [[ 0.80981255], [ 0.87217591], [ 0.9646476 ],
                            [ 0.72368535]],
                           [[ 0.020481  ], [ 0.48838363], [ 0.40267085],
                            [-0.21589186]],
                           [[ 0.7572407 ], [ 0.85899973], [ 1.02198502],
                            [ 0.92879966]],
                           [[ 0.81094342], [ 0.3162253 ], [-0.02894007],
                            [ 0.54357872]]])

        rot = Rotator(3, 10, 20, 30)
        ks = rot.rotate_eigenvector(self.ks, self.lvec)
        self.assertTrue(np.allclose(ks, ks_rot))
